/*
 Navicat Premium Data Transfer

 Source Server         : __localhost_root
 Source Server Type    : MySQL
 Source Server Version : 50638
 Source Host           : localhost:3306
 Source Schema         : logiclike

 Target Server Type    : MySQL
 Target Server Version : 50638
 File Encoding         : 65001

 Date: 20/11/2018 18:49:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for apicalls
-- ----------------------------
DROP TABLE IF EXISTS `apicalls`;
CREATE TABLE `apicalls`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip` int(10) UNSIGNED NULL DEFAULT NULL,
  `ip_string` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `date_add` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for iplist
-- ----------------------------
DROP TABLE IF EXISTS `iplist`;
CREATE TABLE `iplist`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip` int(10) UNSIGNED NULL DEFAULT NULL,
  `ip_string` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_banned` tinyint(1) NULL DEFAULT NULL,
  `date_banned` datetime(0) NULL DEFAULT NULL,
  `lifetime_banned` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `ip`(`ip`) USING BTREE,
  UNIQUE INDEX `ip_string`(`ip_string`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
