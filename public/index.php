<?php

set_time_limit(5);

use Logiclike\Classes\Services\DiService;
use Logiclike\Classes\Services\RequestService;
use Logiclike\Classes\Services\DbService;
use Logiclike\Classes\Services\DProtectionService;
use Logiclike\Classes\Services\RouterService;
use Logiclike\Classes\Services\EventService;


// dirs
define('__PUBLIC__',  realpath(__DIR__));
define('__ROOT__',    realpath(__PUBLIC__ . '/..'));
define('__CONFIG__',  realpath(__ROOT__ . '/config'));


// autoload
require __ROOT__ . '/autoload.php';


// init
$di = DiService::getInstance();

$di->addSingleton('event', function ($di) {
  return new EventService();
});

$di->addSingleton('request', function ($di) {
  return new RequestService();
});

$di->addSingleton('db', function ($di) {
  return new DbService();
});

$di->addSingleton('dprotection', function ($di) {
  return new DProtectionService();
});

$di->addSingleton('router', function ($di) {
  return new RouterService();
});

$event = $di->get('event');
$event->addListener('router:beforeAction', function ($params = null) {
  if (!array_key_exists('api', $params))
    return;

  $router = $this->get('router');
  $dprotection = $this->get('dprotection');

  list($status, $params) = $dprotection->check();
  if (false === $status) {
    $router->mergeParams($params);

    header('X-Banned-Until: ' . $params['X-Banned-Until']);

    return $router->handleError('Forbidden', 403);

  }

  $dprotection->call();
});


// application
$router = $di->get('router');
$router->handle();
