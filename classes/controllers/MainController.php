<?php

namespace Logiclike\Classes\Controllers;

use Logiclike\Classes\Controller;

class MainController extends Controller
{
  public function indexAction($params = null)
  {
    echo 'Index. Works even if ip is blocked';
  }

  public function errorAction($params = null)
  {
    // should be empty?
    die();
  }

  public function methodAction($params = null)
  {
    echo 'Hello World';
  }
}
