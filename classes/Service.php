<?php

namespace Logiclike\Classes;

class Service
{
  protected $_props = [];

  public function hasProp(string $name)
  {
    return array_key_exists($name, $this->_props);
  }

  public function addProp(string $name, $value)
  {
    if ($this->hasProp($name))
      throw new \Exception('Property already declared: ' . $name);

    $this->_props[$name] = $value;
    return $this;
  }

  public function setProp(string $name, $value)
  {
    $this->_props[$name] = $value;
    return $this;
  }

  public function __get($name)
  {
    if (!array_key_exists($name, $this->_props))
      return null;

    return $this->_props[$name];
  }
}
