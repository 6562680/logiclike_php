<?php

namespace Logiclike\Classes\Services;

use Logiclike\Classes\Service;

class DProtectionService extends Service
{
  const CALLS = 5;
  const BANTIME = 600;

  public function check()
  {
    $request = $this->di->get('request');
    $ip = $request->getUserIp();
    $ip_int = ip2long($ip);

    $dt_now = new \DateTime('now');
    $dt_now->setTimezone(new \DateTimeZone('UTC'));

    $pdo = $this->di->get('db')->getPdo();

    // find existing iplist
    $sql = 'SELECT id, is_banned, date_banned, lifetime_banned FROM iplist WHERE ip = ?;';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
      $ip_int,
    ]);

    if (!$iplist_row = $stmt->fetch())
      return [ true, null ];


    // status banned
    if ($iplist_row['is_banned']) {
      $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $iplist_row['date_banned']);
      $dt->modify('+' . $iplist_row['lifetime_banned'] . ' seconds');

      // if ban expired
      $interval = $dt_now->diff($dt);
      if ($interval->invert) {
        $sql = 'UPDATE iplist SET is_banned = ?, date_banned = ?, lifetime_banned = ? WHERE ip = ?;';
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
          null,
          null,
          null,
          $ip_int
        ]);

      } else {
        return [ false, [
          'X-Banned-Until' => $dt->format(\DateTimeInterface::ISO8601)
        ]];

      }
    }


    // calculate count of api calls
    $dt = new \DateTime('now');
    $dt->setTimezone(new \DateTimeZone('UTC'));
    $dt->modify('-2 minutes');
    $dt->add(new \DateInterval('PT' . (60 - $dt->format('s')) . 'S'));

    $sql = 'SELECT COUNT(id) `cnt` FROM apicalls WHERE ip = ? AND date_add > ?;';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
      $ip_int,
      $dt->format('Y-m-d H:i:s'),
    ]);

    $row = $stmt->fetch();


    // add to banlist
    if (static::CALLS <= $row['cnt']) {
      // set banned
      $sql = 'UPDATE iplist SET is_banned = ?, date_banned = ?, lifetime_banned = ? WHERE id = ?';
      $stmt = $pdo->prepare($sql);
      $stmt->execute([
        1,
        $dt->format('Y-m-d H:i:s'),
        static::BANTIME,
        $iplist_row['id'],
      ]);

      // build status
      $dt->modify('+' . static::BANTIME . ' seconds');
      return [ false, [
        'X-Banned-Until' => $dt->format(\DateTimeInterface::ISO8601)
      ]];
    }

    return [ true, null ];
  }


  public function call()
  {
    $request = $this->di->get('request');
    $ip = $request->getUserIp();
    $ip_int = ip2long($ip);

    $pdo = $this->di->get('db')->getPdo();


    // find existing iplist
    $sql = 'SELECT id FROM iplist WHERE ip = ?';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
      $ip_int,
    ]);

    $iplist_row = $stmt->fetch();


    // remove old apicalls
    $dt = new \DateTime('now');
    $dt->setTimezone(new \DateTimeZone('UTC'));
    $dt->modify('-2 minutes');
    $dt->add(new \DateInterval('PT' . (60 - $dt->format('s')) . 'S'));

    $sql = 'DELETE FROM apicalls WHERE ip = ? AND date_add < ?';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
      $ip_int,
      $dt->format('Y-m-d H:i:s'),
    ]);


    // add iplist
    if (!$iplist_row) {
      $sql = 'INSERT INTO iplist(ip, ip_string) VALUES (?, ?)';
      $stmt = $pdo->prepare($sql);
      $stmt->execute([
        $ip_int,
        $ip,
      ]);

    }

    // add apicall
    $dt = new \DateTime('now');
    $dt->setTimezone(new \DateTimeZone('UTC'));

    $sql = 'INSERT INTO apicalls(ip, ip_string, date_add) VALUES (?, ?, ?);';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
      $ip_int,
      $ip,
      $dt->format('Y-m-d H:i:s'),
    ]);
  }
}
