<?php

namespace Logiclike\Classes\Services;

use Logiclike\Classes\Service;

class DbService extends Service
{
  const HOST     = '127.0.0.1';
  const DB       = 'logiclike';
  const USER     = 'root';
  const PASS     = '';
  const CHARSET  = 'utf8mb4';

  protected $_pdo;

  public function getPdo()
  {
    return $this->_pdo;
  }

  public function __construct()
  {
    $host     = static::HOST;
    $db       = static::DB;
    $user     = static::USER;
    $pass     = static::PASS;
    $charset  = static::CHARSET;

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = [
      \PDO::ATTR_ERRMODE             => \PDO::ERRMODE_EXCEPTION,
      \PDO::ATTR_DEFAULT_FETCH_MODE  => \PDO::FETCH_ASSOC,
      \PDO::ATTR_EMULATE_PREPARES    => false,
      \PDO::MYSQL_ATTR_INIT_COMMAND  => 'SET names ' . $charset,
    ];

    $this->_pdo = new \PDO($dsn, $user, $pass, $opt);
  }
}
