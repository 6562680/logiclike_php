<?php

namespace Logiclike\Classes\Services;

use Logiclike\Classes\Service;

class EventService extends Service
{
  protected $_events = [];

  public function has(string $key)
  {
    return array_key_exists($key, $this->_events);
  }

  public function fire(string $key, array $params = [])
  {
    if (!array_key_exists($key, $this->_events))
      return;

    foreach ($this->_events[$key] as $callback)
      $callback($params);
  }

  public function addListener(string $key, callable $callback)
  {
    $this->_events[$key][] = \Closure::bind($callback, $this->di);

    return $this;
  }
}
