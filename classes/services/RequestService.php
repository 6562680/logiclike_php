<?php

namespace Logiclike\Classes\Services;

use Logiclike\Classes\Service;

class RequestService extends Service
{
  public function get(string $name = null)
  {
    if ($name) {
      if (!array_key_exists($name, $_REQUEST))
        return null;

      return $_REQUEST[$name];
    }

    return $_REQUEST;
  }

  public function getQuery(string $name = null)
  {
    if ($name) {
      if (!array_key_exists($name, $_GET))
        return null;

      return $_GET[$name];
    }

    return $_GET;
  }

  public function getPost(string $name = null)
  {
    if ($name) {
      if (!array_key_exists($name, $_POST))
        return null;

      return $_POST[$name];
    }

    return $_POST;
  }

  public function getUserIp()
  {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
      return $_SERVER['HTTP_CLIENT_IP'];

    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
      return $_SERVER['HTTP_X_FORWARDED_FOR'];

    else
      return $_SERVER['REMOTE_ADDR'];
  }
}
