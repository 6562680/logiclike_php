<?php

namespace Logiclike\Classes\Services;

use Logiclike\Classes\Service;

Class DiService extends Service
{
  protected static $_instances = [];

  private function __construct() {
    $this->_singletons['di'] = $this;
    $this->_services['di'] = function () {
      return $this;
    };
  }
  private function __clone() {}
  private function __wakeup() {}

  public static function getInstance()
  {
    $cl = get_called_class();
    if (!array_key_exists($cl, static::$_instances)) {
      $obj = static::$_instances[$cl] = new static();
    }

    return static::$_instances[$cl];
  }


  // services
  protected $_services = [];

  public function has(string $key)
  {
    return array_key_exists($key, $this->_services);
  }

  public function add(string $key, callable $callback)
  {
    if ($this->has($key))
      throw new Exception('Already registered service by key: ' . $key);

    $this->_services[$key] = $callback;

    return $this;
  }

  public function get(string $key)
  {
    if ($this->hasSingleton($key))
      return $this->getSingleton($key);

    if (!$this->has($key))
      throw new Exception('Service not found by key: ' . $key);

    $obj = $this->_services[$key]($this);
    $obj->addProp('di', $this);

    return $obj;
  }


  // singletons
  protected $_singletons = [];

  public function hasSingleton(string $key)
  {
    return array_key_exists($key, $this->_singletons);
  }

  public function addSingleton(string $key, callable $callback)
  {
    if ($this->has($key))
      throw new Exception('Already registered service by key: ' . $key);

    if ($this->hasSingleton($key))
      throw new Exception('Already registered singleton by key: ' . $key);

    $this->_services[$key] = $callback;

    $obj = $this->_singletons[$key] = $callback($this);
    $obj->addProp('di', $this);

    return $this;
  }

  public function getSingleton(string $key)
  {
    if (!$this->hasSingleton($key)) {
      if (!$this->has($key))
        throw new Exception('Service not found by key: ' . $key);

      $this->_singletons[$key] = $this->_services[$key]();
    }

    return $this->_singletons[$key];
  }
}
