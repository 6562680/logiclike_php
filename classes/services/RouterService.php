<?php

namespace Logiclike\Classes\Services;

use Logiclike\Classes\Service;

class RouterService extends Service
{
  protected $_namespace = 'Logiclike\Classes\Controllers';

  protected $_default_controller = 'main';
  protected $_default_action = 'index';

  protected $_error_controller = 'main';
  protected $_error_action = 'error';

  protected $_controller;
  protected $_action;

  protected $_routes = [];
  public function __construct()
  {
    $this->_routes = require __CONFIG__ . '/routes.php';

    foreach ($this->_routes as $index => $route) {
      $path = $route[0];

      $subjects = [];
      $replacements = [];
      $params = [];

      $str = $path;
      $num = 0;
      do {
        $from = strpos($str, '{');
        $to = strpos($str, '}');

        if (false === $from)
          break;

        if (false === $to)
          throw new \Exception('There should be end tag to build regex in route: ' . $path);

        $regex = substr($str, $from, $to - $from + 1);

        $arr = explode(':', trim($regex, '{}'));
        if ($arr[0] && isset($arr[1])) {
          $subjects[ $regex ] = '@' . $num;
          $replacements[ '@' . $num ] = sprintf('(?<%s>%s)', $arr[0], $arr[1]);

        } else {
          $subjects[ $regex ] = '@' . $num;
          $replacements[ '@' . $num ] = sprintf('(%s)', $arr[0]);

        }

        $str = substr($str, $to + 1);
        $num++;
      } while (false !== $from);

      if ($subjects)
        $path = strtr($path, $subjects);

      $route = '~^' . preg_quote(rtrim($path, '/'), '/') . preg_quote('/') . '?$~i';
      $route = strtr($route, $replacements);

      $this->_routes[$index]['regex'] = $route;
    }
  }


  // error
  public function setError(string $controller, string $action)
  {
    $this->_error_controller = $controller;
    $this->_error_action = $action;

    return $this;
  }
  public function getErrorController()
  {
    return $this->_error_controller;
  }
  public function getErrorAction()
  {
    return $this->_error_action;
  }


  // default
  public function setDefault(string $controller, string $action)
  {
    $this->_default_controller = $controller;
    $this->_default_action = $action;

    return $this;
  }
  public function getDefaultController()
  {
    return $this->_default_controller;
  }
  public function getDefaultAction()
  {
    return $this->_default_action;
  }


  // current
  public function getController()
  {
    return $this->_controller;
  }
  public function getAction()
  {
    return $this->_action;
  }


  // params
  protected $_params = [];
  public function setParams(array $params)
  {
    $this->_params = $params;
    return $this;
  }
  public function mergeParams(array $params)
  {
    $this->_params = array_replace($this->_params, $params);
    return $this;
  }
  public function getParam(string $name = null)
  {
    if (!$name)
      return null;

    if (!array_key_exists($name, $this->_params))
      return null;

    return $this->_params[$name];
  }
  public function getParams()
  {
    return $this->_params;
  }


  // namespace
  public function setNamespace(string $namespace)
  {
    $this->_namespace = $namespace;
    return $this;
  }
  public function getNamespace()
  {
    return $this->_namespace;
  }


  // handleError
  public function handleError(string $message, int $code = 500)
  {
    $controller = $this->getErrorController();
    $action = $this->getErrorAction();

    $this->setParams($params = [
      'message' => $message
    ]);

    $cl = $this->getNamespace() . '\\' . $controller . 'Controller';

    $obj = new $cl();
    $obj->setProp('di', $this->di);

    $action .= 'Action';

    http_response_code($code);
    return call_user_func_array([ $cl, $action ], $params);
  }


  // handle
  public function handle()
  {
    $request = $this->di->get('request');
    $url = $request->getQuery('_url') ?: '/';

    $found = false;
    foreach ($this->_routes as $route) {
      if (!preg_match($route['regex'], $url, $m))
        continue;

      $found = true;
      break;
    }

    if (!$found)
      return $this->handleError('Controller not found by path: ' . $url);

    $controller = $route[1] ?: $this->getDefaultController();
    $action = $route[2] ?: $this->getDefaultAction();

    $this->setParams($m);

    $cl = $this->getNamespace() . '\\' . $controller . 'Controller';

    if (!class_exists($cl))
      return $this->handleError('Controller class not found: ' . $cl);

    $obj = new $cl();
    $obj->setProp('di', $this->di);

    $action .= 'Action';
    if (!method_exists($obj, $action))
      return $this->handleError('Action method not found: ' . $action);

    $event = $this->di->get('event');

    if ($event->has('router:beforeAction'))
      $event->fire('router:beforeAction', $this->getParams());

    return call_user_func_array([ $cl, $action ], $m);
  }
}
